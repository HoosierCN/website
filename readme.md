1/16 Update
===========

#### Updates
 - Refactored folder structure, adding client-facing files in the public folder
 - Added the PUG templating engine for HTML (see .pug files)
 - Switched from SystemJS to Webpack allowing a more robust build system rather than just a module server
 - Added .gitignore to ignore "node_modules" and editor config files as is canonical in Node.js development
 - Added webpack-dev-server to hotload front-end changes. Activate via "npm start."
 - Separated vendor code (Angular and polyfills) from application code
 - Removed all compiled files (will be compiled in memory and output via bundles with webpack)

#### Todo
- Convert css to sass and add preprocessor to webpack loader config
- Insert css (sass) directly into js as per webpack specs
- Create more robust commenting
- Add compression and minification
.
- Add #bod, #join, #initiatives sections of page to jump to sections of staff, contact, about pages.
- Add contact page inc to all the angular files