import { Component } from '@angular/core';

@Component({
  selector: 'hcnheader',
  template: `<table bgcolor=red><tbody><tr><td>

  <nav>
      <a routerLink="/home" routerLinkActive="active">Home</a>
      <a [routerLink]="['/story']">Story</a>
      <a routerLink="/staff" routerLinkActive="active">Staff</a>
      <a routerLink="/cc" routerLinkActive="active">CC</a>
      <a routerLink="/iii" routerLinkActive="active">III</a>
      <a routerLink="/ttf" routerLinkActive="active">TTF</a>
    </nav>
  <router-outlet></router-outlet>
  </td></tr></tbody></table>`,
})
export class HeaderComponent  { };
