import { Component } from '@angular/core';

@Component({
  selector: 'styles',
  template: `<style>
@import url('https://fonts.googleapis.com/css?family=Lora:400,400i|Raleway');       

.parallax{
	background-attachment: fixed;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
}

#catalyzing {
	background-image: url("http://i.imgur.com/CkkUXPN.jpg");
	width: 100%;
	height: 300px;
}

p {font-family: 'Raleway', 'Helvetica',sans-serif;}

h1 {font-family: 'Lora','Raleway','Helvetica',sans-serif;
   text-align:center;
   line-height: 20%;
   }
   
h2 {font-family: 'Raleway','Helvetica',sans-serif;
   text-align:center;
   font-size: 1em;
}
   
h3 {font-family:'Raleway','Helvetica',sans-serif;
   text-align:center;
   line-height: 90%;
   }
  </style>`,
})
export class StyleComponent  { };
