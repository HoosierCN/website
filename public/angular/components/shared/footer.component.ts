import { Component } from '@angular/core';

@Component({
  selector: 'hcnfooter',
  template: `    
    <table width="100%" bgcolor="#e0e0e0"><tbody>
    <tr>
    <td>
        <a routerLink="#home" routerLinkActive="active">HCN home</a>
        <br /><a routerLink="#story" routerLinkActive="active">Our impact</a>
        <br /><a routerLink="#staff" routerLinkActive="active">Our team</a>

    <td>
        <a routerLink="#initiatives" routerLinkActive="active">Our initiatives</a>
        <br /><a routerLink="#bod" routerLinkActive="active">HCN Board of Directors</a>
        <br /><a routerLink="#join" routerLinkActive="active">Our initiatives</a>

    <td>

    <table><tbody><tr><td align=center colspan=3>
    <a href="https://www.paypal.com/donate/?token=LmDh7ibB3BSQoQzEYaZJo8SNxbHggX6qhI3JjaRDihD4YDn03wCczyQbVznOuJsKpcKzqW"><button name="Donate" >Donate</button></a>
    <br /></td></tr><tr>
    <td>
        <a href="https://www.linkedin.com/company/hoosiercn">
        <img src="https://static.wixstatic.com/media/e8b742_2fca94b39ca042e4ac0fd9dc6d1abdc1~mv2.png/v1/fill/w_71,h_69,al_c,usm_0.66_1.00_0.01/e8b742_2fca94b39ca042e4ac0fd9dc6d1abdc1~mv2.png" width="20px" height="20px"/></a></td>
    <td>
        <a href="https://www.facebook.com/hoosiercn/">
        <img src="https://www.facebook.com/images/fb_icon_325x325.png" width="20px" height="20px"/>
        </a>
    </td>
    <td>
        <a href="https://www.facebook.com/hoosiercn/">
        <img src="https://www.youtube.com/yt/brand/media/image/YouTube-icon-full_color.png" width="20px" height="20px"/>
        </a>
    </td>
    </tr>
    <tr><td colspan=3><a href="/contactus">Contact us</a></td></tr>
    </tbody></table>
    
    </td>
    </tr><tr><td colspan="3">
    </td></tr></tbody></table>

`,
})
export class FooterComponent  {  }
