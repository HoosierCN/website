import { Component } from '@angular/core';

export class staffmembers  {
  name: string;
  photo: string;
  title: string;
  description: string;
  linkedin: string;
  isleader:boolean;
  }

const STAFFL: staffmembers[]=[
{name:'Josh Knopman',
  photo:'http://www.clker.com/cliparts/p/w/s/A/V/6/green-square-md.png',
  title: 'Chief Executive Officer',
  description:'Josh is a member of the Class of 2017 pursuing a triple major in Supply Chain Management, Marketing, and Technology Management. He has been with HCN since the beginning and is intent on leaving a lasting impact in the community.',
  linkedin:'https://www.linkedin.com/in/joshuaknopman',
  isleader:true,
},

{name:'Jack Langston',
  photo:'http://www.clker.com/cliparts/p/w/s/A/V/6/green-square-md.png',
  title: 'Chief Operations Officer',
  description:'Jack is a Class of 2018 Wells Scholar pursuing degrees in both the Kelley School of Business as well as the School of Informatics and Computing. Jack passionately pursues opportunities that bring students together to make an impact on the world and community around them.',
  linkedin:'https://www.linkedin.com/in/jacklangston',
  isleader:true,
},

{name:'Hannah Eli',
  photo:'http://www.clker.com/cliparts/p/w/s/A/V/6/green-square-md.png',
  title: 'Chief Marketing Officer',
  description:'Hannah is a Class of 2019 Fry Scholar studying Marketing, Business Analytics, Operations, and Spanish. She believes in the positive impact that good brand can have on the ability of a business to achieve its goals.',
  linkedin:'https://www.linkedin.com/in/hannaheli',
  isleader:true,
},

{name:'Austin Peters',
  photo:'http://www.clker.com/cliparts/p/w/s/A/V/6/green-square-md.png',
  title: 'Chief Financial Officer',
  description:'Austin is a member of the Class of 2019 studying Finance, Chinese, and Spanish. He joined HCN both to improve his community and to facilitate other students\' impact in Bloomington and around the world.',
  linkedin:'https://www.linkedin.com/in/peteraus',
  isleader:true,
},

{name:'Carmen Carigan',
  photo:'http://www.clker.com/cliparts/p/w/s/A/V/6/green-square-md.png',
  title: 'Chief Financial Officer',
  description:'Carmen is a class of 2019 Wells Scholar working towards degrees in both SPEA and the Media School, as well as a certificate in Applied Research and Inquiry. Carmen believes that tasks, no matter how big or small, can be accomplished best through the utilization of the talents of man',
  linkedin:'https://www.linkedin.com/in/ccarigan',
  isleader:true,
},

{name:'Will Hopkins',
  photo:'http://www.clker.com/cliparts/p/w/s/A/V/6/green-square-md.png',
  title: 'Finance Associate',
  description:'Will is a member of the Kelley School of Business class of 2020 majoring in Finance, Accounting, and Informatics, and minoring in International Studies. He joined HCN because he believes that students have a common drive for impact, and together they can leave a lasting mark in our community.',
  linkedin:'https://www.linkedin.com/in/williamfowlerhopkins',
  isleader:false,
},

{name:'Sarah Cougill',
  photo:'http://www.clker.com/cliparts/p/w/s/A/V/6/green-square-md.png',
  title: 'Marketing Associate',
  description:'Sarah is a member of the Class of 2019 studying Marketing, Law and Public Policy, and Spanish. She can almost always be found with a camera in hand, as she believes good visual brand is instrumental in the success of any business.',
  linkedin:'https://www.linkedin.com/in/sarahcougill',
  isleader:false,
},

{name:'Cecilia Brisuda',
  photo:'http://www.clker.com/cliparts/p/w/s/A/V/6/green-square-md.png',
  title: 'Marketing Associate',
  description:'Cecilia is a Class of 2020 Wells Scholar pursuing degrees in business and in mathematics. Cecilia strongly believes in the potential of technology to act as a catalyst for change and is enthusiastic about applying her tech knowledge to create positive change.',
  linkedin:'https://www.linkedin.com/in/cbrisuda',
  isleader:false,
}
]

const STAFF: staffmembers[]=[
{name:'Will Hopkins',
  photo:'http://www.clker.com/cliparts/p/w/s/A/V/6/green-square-md.png',
  title: 'Finance Associate',
  description:'Will is a member of the Kelley School of Business class of 2020 majoring in Finance, Accounting, and Informatics, and minoring in International Studies. He joined HCN because he believes that students have a common drive for impact, and together they can leave a lasting mark in our community.',
  linkedin:'https://www.linkedin.com/in/williamfowlerhopkins',
  isleader:false,
},

{name:'Sarah Cougill',
  photo:'http://www.clker.com/cliparts/p/w/s/A/V/6/green-square-md.png',
  title: 'Marketing Associate',
  description:'Sarah is a member of the Class of 2019 studying Marketing, Law and Public Policy, and Spanish. She can almost always be found with a camera in hand, as she believes good visual brand is instrumental in the success of any business.',
  linkedin:'https://www.linkedin.com/in/sarahcougill',
  isleader:false,
},

{name:'Cecilia Brisuda',
  photo:'http://www.clker.com/cliparts/p/w/s/A/V/6/green-square-md.png',
  title: 'Marketing Associate',
  description:'Cecilia is a Class of 2020 Wells Scholar pursuing degrees in business and in mathematics. Cecilia strongly believes in the potential of technology to act as a catalyst for change and is enthusiastic about applying her tech knowledge to create positive change.',
  linkedin:'https://www.linkedin.com/in/cbrisuda',
  isleader:false,
}
]

@Component({
  selector: 'staff',
  template: `
<div *ngFor="let person of staffl" style="position:relative;left: 15%;">
<table><tbody><tr><td>
<img src="{{photo}}"></td>
<td><div style="display: inline-block; width: 25%;"><h2>{{person.name}}</h2><h3>{{person.title}}</h3><p>{{person.description}}</p></div></td>
<td><a href={{person.linkedin}}><img src="https://static.wixstatic.com/media/e8b742_2fca94b39ca042e4ac0fd9dc6d1abdc1~mv2.png/v1/fill/w_64,h_68,al_c,usm_0.66_1.00_0.01/e8b742_2fca94b39ca042e4ac0fd9dc6d1abdc1~mv2.png"></a></td>
</tbody></table>
</div>
  `,
})



export class StaffComponent  {
    staffl=STAFFL;
    staff=STAFF;
  }
