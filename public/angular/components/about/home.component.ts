import { Component } from '@angular/core';

@Component({
  selector: 'hcnhome',
  template: `
  <!-- Four sections: Catalyzing student impact, what we do, our initiatives, our impact -->
<link href="https://fonts.googleapis.com/css?family=Lora:400,400i|Raleway:200i,400,400i,700,700i,800" rel="stylesheet">

<div id="catalyzing" class="parallax" style="position:relative;">
    <p id="catalyzing" class="parallax" align="right"><span style="position:absolute;right:50px;bottom:25px;">
        <font size="15em" color="white"><strong>catalyzing student impact</strong></font><br />
        <font size="5em" color="white">Learn about our initiatives</font>
    </span></p>
</div>
<hr />

<div id="homewhatdo" width="100%">
    <h1><i>What We Do</i></h1>
    <h2>Find out how HCN applies creates opportunities for students to solve problems in the community.</h2>
    <div style="width:90%; position:relative;left:10%;border-width:thin;border-style: solid;">
        <div style="width: 30%; height: 200px; border-color: #000000; border-width: 1px; border-style: solid; overflow: auto; display:table-cell;">
            <h2>We believe students have a common passion: impact.</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis</p>
            <p align=right>Learn more...</p>
        </div>
        <div class=parallax style="background-image: url('https://i.ytimg.com/vi/SNggmeilXDQ/maxresdefault.jpg'); display:table-cell;" width="100%" height="100%">_</div>
    </div>

</div>
  `,
})
export class HomeComponent  {  }
