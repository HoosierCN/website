import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent }  from './app.component';
import { HeaderComponent } from './components/shared/header.component';
import { StyleComponent } from './components/shared/style.component';
import { FooterComponent } from './components/shared/footer.component';
import { HomeComponent } from './components/about/home.component';
import { StoryComponent } from './components/about/story.component';
import { StaffComponent } from './components/about/staff.component';
import { IiiComponent } from './components/initiatives/iii.component';
import { CCComponent } from './components/initiatives/cc/cc.component';
import { TTFComponent } from './components/initiatives/ttf.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home',  component: HomeComponent },
  { path: 'story', component: StoryComponent },
  { path: 'staff',     component: StaffComponent },
  { path: 'cc',     component: CCComponent },
  { path: 'iii',     component: IiiComponent },
  { path: 'ttf',     component: TTFComponent },
  { path: 'style',     component: StyleComponent },
];

@NgModule({
  imports:      [
    BrowserModule, RouterModule.forRoot(routes) ],
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    StoryComponent,
    StaffComponent,
    IiiComponent,
    TTFComponent,
    CCComponent,
    HomeComponent,
    StyleComponent,
  ],
  bootstrap:    [ AppComponent ]
})


export class AppModule { }
