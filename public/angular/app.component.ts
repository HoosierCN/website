import { Component } from '@angular/core';
import { HeaderComponent } from './components/shared/header.component';
import { FooterComponent } from './components/shared/footer.component';
import { StyleComponent } from './components/shared/style.component';
import { HomeComponent } from './components/about/home.component';
import { StoryComponent } from './components/about/story.component';
import { StaffComponent } from './components/about/staff.component';
import { IiiComponent } from './components/initiatives/iii.component';
import { CCComponent } from './components/initiatives/cc/cc.component';
import { TTFComponent } from './components/initiatives/ttf.component';


@Component({
  selector: 'my-app',
  template: `\

  <hcnheader></hcnheader>
  <router-outlet></router-outlet>
  <hcnfooter></hcnfooter>
  <p>hello world</p>
  `,

})
export class AppComponent  {

}
