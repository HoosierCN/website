const webpack = require('webpack');
const webpackMerge = require('webpack-merge');
const common = require('./webpack.common.js');
const path = require('path');

const rootDir = path.resolve(__dirname, "..");



module.exports = webpackMerge(common, {
    debug: true,
    devServer:{
        contentBase: path.resolve(rootDir, 'dist'),
        port:9000
    },
    devtool: 'source-map'
});