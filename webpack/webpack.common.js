const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');



module.exports = {

    context: path.resolve(__dirname, "../public/"),

    entry: {
        app: './angular/main.ts',
        vendor: ['./vendor/poly.ts', './vendor/vendor.ts']
    },

    output: {
        path: path.resolve(__dirname, '../dist/'),
        filename: '[name].js'
    },

    module: {
        loaders: [

            {
                test: /\.ts$/,
                loaders: ['awesome-typescript-loader', 'angular2-template-loader']
            },
            {
                test: /\.pug$/,
                loader: 'pug-html-loader'
            },

            {
                test: /\.css$/,
                loader: 'style-loader!css-loader'
            }

        ]
    },
    resolve: {
        extensions: ['', '.ts', '.js', '.pug'],
        modulesDirectories: ['node_modules']
    },

    plugins: [
        new webpack.optimize.CommonsChunkPlugin({
            name: ['app', 'vendor']
        }),

        new HtmlWebpackPlugin({
            template: './index.pug'
        })
    ]
};